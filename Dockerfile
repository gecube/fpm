FROM ubuntu

RUN apt-get update && \
    apt -y install ruby ruby-dev rubygems build-essential gcc rpm && \
    gem install --no-ri --no-rdoc fpm

